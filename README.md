## SUMMARY

The Number Double module provides a Drupal field type to support data that needs to be stored in the database as the
MySQL DOUBLE type. For more information on the MySQL DOUBLE type, see: https://dev.mysql.com/doc/refman/5.5/en/floating-point-types.html

## REQUIREMENTS

- Field module (core)
- Number module (core)

## INSTALLATION

Install as usual. See https://www.drupal.org/docs/extending-drupal/installing-modules for further information.

## CONFIGURATION

There is no configuration for this module.

## USAGE

After you install this module, a new field type of 'Double' will be available with a widget of text field. You can add
these fields like any other field to your bundles.

## CONTRIBUTORS

Maintainers
[//]: # "cSpell:disable"
- Mark Boyd (mrkdboyd) - https://www.drupal.org/u/mrkdboyd
- Daniel Mundra (dmundra) - https://drupal.org/u/dmundra
